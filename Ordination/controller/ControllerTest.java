package controller;

import static org.junit.Assert.*;
import java.time.LocalDate;
import java.time.LocalTime;
import org.junit.Before;
import org.junit.Test;
import ordination.DagligFast;
import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;
import storage.Storage;

public class ControllerTest {
	Patient p1, p2, p3;
	Laegemiddel l1;
	Controller c1;
	PN pn1, pn2;

	@Before
	public void setUp() throws Exception {
		c1 = Controller.getController();
		p1 = new Patient("123456-7890", "Jane Jensen", 63.4);
		p2 = new Patient("123456-7891", "Corona Carsten", 130);
		p3 = new Patient("123456-7892", "Corona Conni", 20);
		l1 = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		pn1 = c1.opretPNOrdination(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 2), p1, l1, 1);
		pn2 = c1.opretPNOrdination(LocalDate.of(2019, 1, 2), LocalDate.of(2019, 1, 3), p1, l1, 2);
		
	}

	@Test
	public void testOpretPNOrdination() {
		// giver IllegalArgumentException som den skal
//		PN pn1 = c1.opretPNOrdination(LocalDate.of(2019, 01, 03), LocalDate.of(2019, 01, 01), p1, l1, 2);
//		assertTrue(pn1.getAntalEnheder()==2);
		
		
//		PN pn2 = c1.opretPNOrdination(LocalDate.of(2019, 01, 02), LocalDate.of(2019, 01, 03), p1, l1, 2);
//	assertTrue(pn2.getAntalEnheder()==2);
//		assertEquals(2, pn2.getAntalEnheder(), 0.01);
	}


	@Test
	public void testAnbefaletDosisPrDoegn() {
		assertEquals(9.51, c1.anbefaletDosisPrDoegn(p1, l1), 0.001);
		assertEquals(20.8, c1.anbefaletDosisPrDoegn(p2, l1), 0.001);
		assertEquals(2, c1.anbefaletDosisPrDoegn(p3, l1), 0.001);
	}

//	@Test
//	public void testAntalOrdinationerPrVægtPrLægemiddel() {
//		//virker ikke
//		assertEquals(1, c1.antalOrdinationerPrVægtPrLægemiddel(25, 80, l1));
//		assertEquals(1, c1.antalOrdinationerPrVægtPrLægemiddel(100, 150, l1));
//	}



}
