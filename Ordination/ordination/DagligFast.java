package ordination;

import java.time.*;
//
import java.time.temporal.ChronoUnit;

public class DagligFast extends Ordination {

	private long daysBetween = ChronoUnit.DAYS.between(getStartDen(), getSlutDen()) + 1;
	private Dosis[] doser = new Dosis[4];

	public DagligFast(LocalDate startDen, LocalDate slutDen) {
		super(startDen, slutDen);
	}

	@Override
	public double samletDosis() {
		double samletDosis = 0;

		for (int i = 0; i < doser.length; i++) {
			if (doser[i] != null) {
				samletDosis += doser[i].getAntal();
			}
		}

		return samletDosis * daysBetween;
	}

	@Override
	public double doegnDosis() {

		return samletDosis() / daysBetween;
	}

	public Dosis[] getDoser() {
		return doser;
	}

	public Dosis createDosis(LocalTime tid, double antal) {
		Dosis dosis = new Dosis(tid, antal);

		if (tid.getHour() < 10 && tid.getHour() > 6) {
			doser[0] = dosis;
		} else if (tid.getHour() >= 10 && tid.getHour() < 14) {
			doser[1] = dosis;
		} else if (tid.getHour() >= 14 && tid.getHour() < 20) {
			doser[2] = dosis;
		} else {
			doser[3] = dosis;
		}

		return dosis;
	}

	@Override
	public String getType() {
		return this.getClass().getName();
	}
}
