package ordination;

import static org.junit.Assert.*;
import java.time.LocalDate;
import java.time.LocalTime;
//
import org.junit.Before;
import org.junit.Test;

public class DagligFastTest {

	Patient p1;
	Laegemiddel l1;
	DagligFast df, df1, df2;

	@Before
	public void setUp() throws Exception {
		p1 = new Patient("121256-0512", "Jane Jensen", 63.4);
		l1 = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		df = new DagligFast(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 3));
		df.setLægemiddel(l1);
		p1.addOrdination(df);
		df.createDosis(LocalTime.of(8, 0), 1);
		df.createDosis(LocalTime.of(12, 0), 2);
	}

	@Test
	public void testSamletDosis() {
		df.createDosis(LocalTime.of(18, 00), 3);
		df.createDosis(LocalTime.of(22, 00), 4);

		assertEquals(30, df.samletDosis(), 0.01);

		df1 = new DagligFast(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 3));
		df1.createDosis(LocalTime.of(18, 00), 3);
		df1.createDosis(LocalTime.of(22, 00), 4);

		assertEquals(21, df1.samletDosis(), 0.01);

		df2 = new DagligFast(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 1));
		df2.createDosis(LocalTime.of(18, 00), 3);
		df2.createDosis(LocalTime.of(22, 00), 4);

		assertEquals(7, df2.samletDosis(), 0.01);

	}

	@Test
	public void testDoegnDosis() {
		df.createDosis(LocalTime.of(18, 00), 3);
		df.createDosis(LocalTime.of(22, 00), 4);
		assertEquals(10, df.doegnDosis(), 0.01);
	}

	@Test
	public void testCreateDosis() {
		assertTrue(df.getDoser()[2] == null);
		df.createDosis(LocalTime.of(18, 00), 3);
		assertTrue(df.getDoser()[2].getAntal() == 3);
	}
}
