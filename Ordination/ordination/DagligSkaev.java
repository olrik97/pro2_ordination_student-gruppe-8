package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class DagligSkaev extends Ordination {
	private long daysBetween = ChronoUnit.DAYS.between(getStartDen(), getSlutDen()) + 1;
	// link til dosis
	private ArrayList<Dosis> doser = new ArrayList<>();

	public DagligSkaev(LocalDate startDen, LocalDate slutDen) {
		super(startDen, slutDen);
	}

	public ArrayList<Dosis> getDosis() {
		return new ArrayList<>(doser);
	}

	public Dosis createDosis(LocalTime tid, double antal) {
		Dosis dosis = new Dosis(tid, antal);
		doser.add(dosis);
		return dosis;
	}

	@Override
	public double samletDosis() {
		double samletDosis = 0;

		for (Dosis d : doser) {
			samletDosis += d.getAntal();
		}

		return samletDosis * daysBetween;
	}

	@Override
	public double doegnDosis() {

		return samletDosis() / daysBetween;
	}

	@Override
	public String getType() {
		return this.getClass().getName();
	}
}
