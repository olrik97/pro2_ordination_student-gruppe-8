package ordination;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

public class DagligSkaevTest {

	Patient p1;
	Laegemiddel l1;
	DagligSkaev df, df1, df2;

	@Before
	public void setUp() throws Exception {
		p1 = new Patient("121256-0512", "Jane Jensen", 63.4);
		l1 = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		df = new DagligSkaev(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 3));
		df.setLægemiddel(l1);
		p1.addOrdination(df);
		df.createDosis(LocalTime.of(9, 30), 2);
		df.createDosis(LocalTime.of(10, 30), 1);

	}

	@Test
	public void testSamletDosis() {
		df.createDosis(LocalTime.of(13, 30), 2);
		df.createDosis(LocalTime.of(14, 30), 1);
		df.createDosis(LocalTime.of(19, 30), 2);
		df.createDosis(LocalTime.of(20, 30), 1);
		assertEquals(27, df.samletDosis(), 0.01);

		df1 = new DagligSkaev(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 3));

		df1.createDosis(LocalTime.of(9, 30), 1);
		df1.createDosis(LocalTime.of(10, 30), 2);
		df1.createDosis(LocalTime.of(13, 30), 0);
		df1.createDosis(LocalTime.of(14, 30), 0);

		assertEquals(9, df1.samletDosis(), 0.01);

		df2 = new DagligSkaev(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 1));
		df2.createDosis(LocalTime.of(9, 30), 0);
		df2.createDosis(LocalTime.of(10, 30), 0);
		df2.createDosis(LocalTime.of(13, 30), 3);
		df2.createDosis(LocalTime.of(14, 30), 4);

		assertEquals(7, df2.samletDosis(), 0.01);

	}

	@Test
	public void testDoegnDosis() {
		df.createDosis(LocalTime.of(13, 30), 3);
		df.createDosis(LocalTime.of(14, 30), 4);
		assertEquals(10, df.doegnDosis(), 0.01);

		df1 = new DagligSkaev(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 3));
		df1.createDosis(LocalTime.of(13, 30), 0);
		df1.createDosis(LocalTime.of(14, 30), 0);
		df1.createDosis(LocalTime.of(13, 30), 3);
		df1.createDosis(LocalTime.of(14, 30), 4);
		assertEquals(7, df1.doegnDosis(), 0.01);

	}

	@Test
	public void testCreateDosis() {
		assertTrue(df.getDosis().size() == 2);
		df.createDosis(LocalTime.of(18, 00), 3);
		assertTrue(df.getDosis().size() == 3);
	}
}
