package ordination;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class PN extends Ordination {

	private double antalEnheder;
	private int sum = 0;
	private ArrayList<LocalDate> givesDen = new ArrayList<>();

	public PN(LocalDate startDen, LocalDate slutDen) {
		super(startDen, slutDen);
	}

	/**
	 * Registrerer at der er givet en dosis paa dagen givesDen Returnerer true hvis
	 * givesDen er inden for ordinationens gyldighedsperiode og datoen huskes
	 * Retrurner false ellers og datoen givesDen ignoreres
	 * 
	 * @param givesDen
	 * @return
	 */
	public boolean givDosis(LocalDate givesDen) {
		sum++;

		this.givesDen.add(givesDen);
		if (givesDen.isAfter(getStartDen()) && givesDen.isBefore(getSlutDen())) {
			return true;
		} else {
			return false;
		}
	}

	public double doegnDosis() {

		long antalDøgn = ChronoUnit.DAYS.between(getStartDen(), getSlutDen()) + 1;

		return (sum * antalEnheder) / antalDøgn;
	}

	public double samletDosis() {

		return sum * antalEnheder;
	}

	/**
	 * Returnerer antal gange ordinationen er anvendt
	 * 
	 * @return
	 */
	public int getAntalGangeGivet() {
		return sum;
	}

	public double getAntalEnheder() {
		return antalEnheder;
	}

	@Override
	public String getType() {
		return this.getClass().getName();
	}
	
	public void setAntalEnheder(double antalEnheder) {
		this.antalEnheder = antalEnheder;
	}

}
