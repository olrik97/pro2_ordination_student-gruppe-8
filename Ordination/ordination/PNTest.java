package ordination;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

public class PNTest {

	Patient p1;
	Laegemiddel l1;
	PN PN;
	PN PN2;

	@Before
	public void setUp() throws Exception {

		p1 = new Patient("121256-0512", "Jane Jensen", 63.4);
		l1 = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		PN = new PN(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 3));
		PN2 = new PN(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 3));

		
		PN.setLægemiddel(l1);
		p1.addOrdination(PN);

	}

	@Test
	public void testGivDosis() {
		assertEquals(true, PN.givDosis(LocalDate.of(2019, 1, 2)));

		assertEquals(false, PN.givDosis(LocalDate.of(2019, 1, 5)));

		assertEquals(PN.givDosis(LocalDate.of(2019, 1, 2)), PN.givDosis(LocalDate.of(2019, 1, 2)));

	}

	@Test
	public void testSamletDosis() {

		PN.givDosis(LocalDate.of(2019, 1, 2));

		PN.setAntalEnheder(3);

		assertEquals(3, PN.samletDosis(), 0.01);

		PN2.givDosis(LocalDate.of(2019, 1, 2));
		PN2.givDosis(LocalDate.of(2019, 1, 2));
		PN2.givDosis(LocalDate.of(2019, 1, 2));
		PN2.givDosis(LocalDate.of(2019, 1, 2));

		PN2.setAntalEnheder(1);
	
		assertEquals(4, PN2.samletDosis(), 0.01);

	}

	@Test
	public void testGetAntalGangeGivet() {

		PN.givDosis(LocalDate.of(2019, 1, 2));
		assertEquals(1, PN.getAntalGangeGivet());
	}
	
	@Test
	public void testAntalEnheder() {

		PN.givDosis(LocalDate.of(2019, 1, 2));
		
		PN.setAntalEnheder(20);
		assertEquals(20, PN.getAntalEnheder(),0.01);
	}
	
	

	@Test
	public void testDoegnDosis() {
		
	
		PN2.givDosis(LocalDate.of(2019, 1, 2));
		PN2.givDosis(LocalDate.of(2019, 1, 2));
		PN2.givDosis(LocalDate.of(2019, 1, 2));
		PN2.givDosis(LocalDate.of(2019, 1, 2));
		PN2.givDosis(LocalDate.of(2019, 1, 2));
		PN2.givDosis(LocalDate.of(2019, 1, 2));
		
		PN2.setAntalEnheder(1);
	
		assertEquals(2, PN2.doegnDosis(), 0.01);
	

		PN.givDosis(LocalDate.of(2019, 2, 1));
		
		PN.setAntalEnheder(3);

		assertEquals(1, PN.doegnDosis(), 0.00001);

	}

}
